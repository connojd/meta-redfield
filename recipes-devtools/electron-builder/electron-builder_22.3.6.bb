SUMMARY = "Electron builder"
HOMEPAGE = "https://www.electron.build/"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

PROVIDES = "\
    ${PN} \
"

inherit native npm

SRC_URI = "\
    npm://registry.npmjs.org;name=electron-builder;version=${PV} \
"

# Must be set after inherit npm since that itself sets S
S = "${WORKDIR}/npmpkg"

BBCLASSEXTEND = "native"

do_install_append() {
    rm -f ${D}${libdir}/node
}

FILES_${PN} = "${bindir}/electron-builder"
FILES_${PN}-native = "${bindir}/electron-builder"
