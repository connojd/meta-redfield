# lvmlocal.conf will be used for image specific configurations.
# It is provided empty as a place holder by the upstream package.
do_install_append() {
    rm -f ${D}${sysconfdir}/lvm/lvmlocal.conf
}
