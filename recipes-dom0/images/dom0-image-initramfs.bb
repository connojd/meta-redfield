# Simple initramfs image. Mostly used for live images.
DESCRIPTION = "Redfield dom0 initramfs. Transition to the rootfs stored in \
Redfield LVM/Luks volume after running optional early boot modules."

INITRAMFS_SCRIPTS ?= " \
    initramfs-framework-base \
    initramfs-module-udev \
    initramfs-module-plymouth \
    initramfs-module-redfield-banner \
    initramfs-module-redfield-measure \
    initramfs-module-redfield-lvm \
    initramfs-module-redfield-cryptsetup \
    initramfs-module-redfield-mount \
"

PACKAGE_INSTALL = " \
    ${INITRAMFS_SCRIPTS} \
    ${VIRTUAL-RUNTIME_base-utils} \
    kernel-module-i915 \
    kernel-module-intel-hid \
    linux-firmware-i915 \
    udev \
    base-passwd \
    initramfs-lvm-configs \
    ${ROOTFS_BOOTSTRAP_INSTALL} \
    ykpers \
    yubikey-full-disk-encryption \
"

# Do not pollute the initrd image with rootfs features
IMAGE_FEATURES = ""

export IMAGE_BASENAME = "${MLPREFIX}dom0-image-initramfs"
IMAGE_LINGUAS = ""

LICENSE = "MIT"

IMAGE_FSTYPES = "${INITRAMFS_FSTYPES}"
inherit core-image

IMAGE_ROOTFS_SIZE = "8192"
IMAGE_ROOTFS_EXTRA_SPACE = "0"

# Use the same restriction as initramfs-live-install
COMPATIBLE_HOST = "(i.86|x86_64).*-linux"

BAD_RECOMMENDATIONS += " \
    busybox-syslog \
    initramfs-module-rootfs \
"
