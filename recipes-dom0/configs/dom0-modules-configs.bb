DESCRIPTION = "DOM0 MODULE CONFIGS"

LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

SRC_URI = " \
    file://10-xen-kmod.conf \
    file://20-uinput.conf \
"

FILES_${PN} += " \ 
    /etc/modules-load.d/10-xen-kmod.conf \
    /etc/modules-load.d/20-uinput.conf \
"

do_install() {
    install -d ${D}${sysconfdir}/modules-load.d
    install -m 0644 ${WORKDIR}/10-xen-kmod.conf ${D}${sysconfdir}/modules-load.d
    install -m 0644 ${WORKDIR}/20-uinput.conf ${D}${sysconfdir}/modules-load.d
}

RDEPENDS_${PN} = " \
"
