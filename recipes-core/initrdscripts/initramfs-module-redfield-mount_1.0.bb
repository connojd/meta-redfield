SUMMARY = "initramfs-framework module for Redfield dom0 rootfs mount."
DESCRIPTION = "This module will mount the dom0 rootfs."
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/COPYING.MIT;md5=3da9cfbcb788c80a0384361b4de20420"

PR = "r0"

inherit allarch

FILESEXTRAPATHS_prepend := "${THISDIR}/initramfs-framework:"
SRC_URI = "file://mount-redfield"

S = "${WORKDIR}"

do_install() {
    install -d ${D}/init.d
    install -m 0755 ${WORKDIR}/mount-redfield ${D}/init.d/90-mount-redfield
}

FILES_${PN} = "/init.d/90-mount-redfield"

RDEPENDS_${PN} = " \
    initramfs-framework-base \
    initramfs-module-redfield-conf \
    coreutils \
    kernel-module-overlay \
"

