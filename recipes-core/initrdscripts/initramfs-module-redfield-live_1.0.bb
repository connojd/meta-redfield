SUMMARY = "initramfs-framework module for live booting."
DESCRIPTION = "This module will search mountable medias for the rootfs image to \
boot into and unmount other medias potentialy mounted during the probing."
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/COPYING.MIT;md5=3da9cfbcb788c80a0384361b4de20420"

RDEPENDS_${PN} = "initramfs-framework-base udev-extraconf"

PR = "r0"

inherit allarch

FILESEXTRAPATHS_prepend := "${THISDIR}/initramfs-framework:"
SRC_URI = "file://live-redfield"

S = "${WORKDIR}"

do_install() {
    install -d ${D}/init.d
    install -m 0755 ${WORKDIR}/live-redfield ${D}/init.d/80-live-redfield
}

FILES_${PN} = "/init.d/80-live-redfield"
