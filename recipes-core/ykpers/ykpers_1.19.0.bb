SUMMARY = "Yubikey Personilization Tools"

HOMEPAGE = "https://www.yubico.com/products/services-software/download/yubikey-personalization-tools"
LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://COPYING;md5=422217170b70a9a94c32873055b486bb"

SRC_URI = " \
    https://developers.yubico.com/yubikey-personalization/Releases/${PN}-${PV}.tar.gz \
"

DEPENDS += " \
    libusb \
    libyubikey \
"

inherit autotools pkgconfig

SRC_URI[md5sum] = "22084c0e55f3c3259963c0136adb3950"
SRC_URI[sha256sum] = "2bc8afa16d495a486582bad916d16de1f67c0cce9bb0a35c3123376c2d609480"

