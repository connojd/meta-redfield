DESCRIPTION = "Graphic installer live image."
LICENSE = "MIT"

require installer-netinst-image.bb

IMAGE_INSTALL += " \
    dom0 \
    ndvm \
"

image_postprocess_installer_enable_tty4() {
    systemctl --root ${IMAGE_ROOTFS} enable getty@tty4.service
}

IMAGE_ROOTFS_EXTRA_SPACE += " + 32768 "

image_postprocess_installer() {
    mkdir -p ${IMAGE_ROOTFS}/${sysconfdir}/installer.d

    cat ${IMAGE_ROOTFS}/${sysconfdir}/installer.d/* >> ${IMAGE_ROOTFS}/${sysconfdir}/installer.yaml
}

ROOTFS_POSTPROCESS_COMMAND_append = " \
    image_postprocess_installer_enable_tty4; \
    image_postprocess_installer; \
"
