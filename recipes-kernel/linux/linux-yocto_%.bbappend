FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"

SRC_URI += " \
           file://redfield-dom0.scc \
           "

KERNEL_MODULE_AUTOLOAD += "br-netfilter"
KERNEL_MODULE_AUTOLOAD += "bridge"
KERNEL_MODULE_AUTOLOAD += "xen-gntdev"
KERNEL_MODULE_AUTOLOAD += "xen-gntalloc"
KERNEL_MODULE_AUTOLOAD += "xen-pciback"
KERNEL_MODULE_AUTOLOAD += "xen-acpi-processor"
KERNEL_MODULE_AUTOLOAD += "xen-wdt"
KERNEL_MODULE_AUTOLOAD += "xen-netback"
KERNEL_MODULE_AUTOLOAD += "xen-blkback"
