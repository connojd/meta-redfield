DEPENDS += "${@bb.utils.contains('ARCH', 'x86', 'elfutils-native', '', d)}"
DEPENDS += "openssl-native util-linux-native"

COMPATIBLE_MACHINE = "qemuarm|qemuarm64|qemux86|qemuppc|qemumips|qemumips64|qemux86-64|intel-corei7"

KBRANCH ?= "master"
LINUX_VERSION ?= "4.18.12"
SRCREV ?= "418669ac6ebdfdddacd4fe4f86d8959cfef27c67"

SRC_URI = " \
           git://kernel.ubuntu.com/ubuntu/ubuntu-cosmic.git;branch=${KBRANCH} \
           file://defconfig \
"

PV = "${LINUX_VERSION}+git${SRCPV}"

inherit kernel
require recipes-kernel/linux/linux-yocto.inc

LIC_FILES_CHKSUM = "file://LICENSES/preferred/GPL-2.0;md5=e6a75371ba4d16749254a51215d13f97"

do_compile_prepend() {
    cp ${WORKDIR}/defconfig ${B}/.config
    cp ${S}/debian/scripts/retpoline-extract-one ${S}/scripts/ubuntu-retpoline-extract-one
}
