DESCRIPTION = "NDVM NETCTL CONFIGS"

LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

SRC_URI = " \
    file://netctl-front.env \
"

do_install() {
    install -d ${D}${sysconfdir}
    install -m 0644 ${WORKDIR}/netctl-front.env ${D}${sysconfdir}
}

RDEPENDS_${PN} = " \
    go-netctl-front \
"
